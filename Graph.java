/*Kathryn Hochberg
 * khochbe1
 * 600.226
 * Project 5
 */

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**Network class, which handles the adjacency list of the 
 * network as well as the lookup table. 
 * @author Kathryn
 */
public class Graph {

    /**The lookup table.*/
    private ArrayList<Vertex> lookUp; 
    /**The number of vertices.*/
    private int vertices; 
    /**The array of arraylists.*/
    private ArrayList<ArrayList<Edge>> adjList;
    /**The clusters of non-adjacent vertices.*/
    private ArrayList<ArrayList<Vertex>> clusters; 

    /**Constructor.*/
    public Graph() {
        this.lookUp = new ArrayList<Vertex>(); 
        this.vertices = 0;
        this.adjList = new ArrayList<ArrayList<Edge>>();
    }

    /**Puts all the information from a line of input 
     * into the look up table and adjacency list. 
     * @param input the input 
     */
    public void newEntry(String input) {
        int i = input.indexOf(' ');
        String s1 = input.substring(0, i);
        input = (input.substring(i)).trim(); 
        i = input.indexOf(' ');
        String s2 = input.substring(0, i);
        input = (input.substring(i)).trim(); 
        int bw = Integer.parseInt(input);
        int i1 = this.add(s1);
        int i2 = this.add(s2);
        Vertex v1 = this.lookUp.get(i1);
        Vertex v2 = this.lookUp.get(i2);
        Edge e = new Edge(v1, v2, bw);
        this.adjList.get(i1).add(e);
        this.adjList.get(i2).add(e);
    }

    /**Will create and add vertex to lookUp table if not created
     * already, if already created will return the ID of the vertex.
     * @param ip the IP address
     * @return the ID of the vertex
     */
    public int add(String ip) {
        int id = this.contains(ip);
        if (id == -1) {
            Vertex v = new Vertex(ip);
            this.lookUp.add(v);
            ArrayList<Edge> a = new ArrayList<Edge>(); 
            this.adjList.add(a);
            this.vertices++;
            return v.id(); 
        }
        return id;
    }

    /**To look if there is a vertex created for the IP address.
     * @param ip the IP address
     * @return the ID of the vertex if found, else -1
     */
    public int contains(String ip) {
        for (int i = 0; i < this.vertices; i++) {
            if (this.lookUp.get(i).data().equals(ip)) {
                return i; 
            }
        }
        return -1; 
    }

    /**Returns string for the lookUp table. 
     * @param out the PrintWriter 
     */
    public void printLookUp(PrintWriter out) {
        out.println("ID\t\t\tIP Address"); 
        int i = 0; 
        while (i < this.vertices) {
            out.println(i + "\t\t\t" + this.lookUp.get(i).data());
            i++;
        }
    }

    /**Prints the representation of the network's adjacency list.
     * @param out the PrintWriter 
     */
    public void printAdjList(PrintWriter out) { 
        for (int i = 0; i < this.vertices; i++) {
            out.print(i + ": ");
            int k = 0;
            for (Edge e : this.adjList.get(i)) {
                if (e.source().id() == i) {
                    out.print(e.end());
                } else {
                    out.print(e.source());
                }
                if (k < this.adjList.get(i).size() - 1) {
                    out.print(", ");
                }
                k++;
            }
            out.println();
        } 
    }

    /**Finds and prints the edges of the maximum spanning tree.
     * @param out the PrintWriter
     */
    public void maximumSpanningTree(PrintWriter out) {
        ArrayList<Edge> edgeList = new ArrayList<Edge>(); 
        for (ArrayList<Edge> ra : this.adjList) {
            for (Edge e : ra) {
                edgeList.add(e);
            }
        }
        edgeList.trimToSize(); 
        Collections.sort(edgeList);
        Set<Vertex> set = new Set<Vertex>(this.lookUp);
        for (Edge e : edgeList) {
            if (set.union(e.source(), e.end())) {
                out.print(e.toString() +  " "); 
            }
        }
    }

    /**Prints the routing table. 
     * @param out the PrintWriter
     */
    public void routingTable(PrintWriter out) {
        Vertex[] v;
        for (int i = 0; i < this.vertices; i++) {
            out.print(i + ": [");
            v = this.findShortestPaths(this.lookUp.get(i));
            int j = 0;
            while (j < this.vertices && v[j] != null) {
                out.print(v[j]);
                if (j < this.vertices - 1) {
                    out.print(", "); 
                }
                j++;
            }
            out.print("]\n");
        }
    }

    /**Finds the shortest route to each of the other vertices 
     * from a starting vertex. 
     * @param s the source 
     * @return the array that contains the previous vertex in the 
     * shortest path from 
     * the starting vertex
     */
    public Vertex[] findShortestPaths(Vertex s) {
        PriorityQueue<Vertex> pq = new PriorityQueue<Vertex>(); 
        Vertex[] previous = new Vertex[this.vertices]; 
        int[] distance = new int[this.vertices];
        int[] minBW = new int[this.vertices];

        Arrays.fill(previous, null);
        Arrays.fill(distance, Integer.MAX_VALUE); 
        Arrays.fill(minBW, 0);

        previous[s.id()] = s;
        distance[s.id()] = 0; 
        minBW[s.id()] = Integer.MAX_VALUE; 

        //Filling the priority queue    
        for (Vertex i : this.lookUp) {
            if (i != null) {
                pq.insert(0, 0, i);
            }
        }
        pq.changeKey(s, 1, 0);
        Vertex v = s; 
        int curr = 1; //keeps track of how far you are from the original node 
        while (!pq.isEmpty()) {
            v = pq.remove(); 
            for (Edge e : this.adjList.get(v.id())) {
                Vertex w = e.getOther(v);
                int alt = distance[v.id()] + 1; 
                if (alt < distance[w.id()]) {
                    distance[w.id()] = alt; 
                    minBW[w.id()] = this.getLower(e.weight(), minBW[v.id()]);
                    previous[w.id()] = v; 
                    pq.changeKey(w, curr, e.weight());
                } else if (alt == distance[w.id()]) {
                    alt = this.getLower(e.weight(), minBW[v.id()]); 
                    if (alt > minBW[w.id()]) {
                        minBW[w.id()] = alt; 
                        previous[w.id()] = v; 
                    }
                }
            }
            curr++;
        }
        return previous; 
    }

    /**Given two integer values, returns the lower one. 
     * @param int1 the first integer
     * @param int2 the second integer
     * @return the lower integer
     */
    //Only used to reduce the cyclomatic complexity of findShortestPath function
    public int getLower(int int1, int int2) {
        if (int1 < int2) {
            return int1;
        } 
        return int2; 
    }

    /**Creates clusters of non-adjacent vertices.
     */
    public void makeClusters() {
        PriorityQueue<Vertex> pq = new PriorityQueue<Vertex>();
        this.clusters = new ArrayList<ArrayList<Vertex>>(); 
        for (int i = 0; i < this.vertices; i++) {
            pq.insert(this.adjList.get(this.lookUp.get(i).id()).size(), 
                    this.lookUp.get(i).id(), this.lookUp.get(i));
            ArrayList<Vertex> ra = new ArrayList<Vertex>(); 
            this.clusters.add(ra);
        }
        this.lookUp.trimToSize(); 
        boolean nonAdj = false; 
        Vertex v; 
        while (!pq.isEmpty()) {
            v = pq.remove(); 
            for (int j = 0; j < this.vertices; j++) {
                nonAdj = false; 
                if (this.clusters.get(j).isEmpty()) {
                    this.clusters.get(j).add(v);
                    break;
                } else {
                    for (Vertex w : this.clusters.get(j)) {
                        if (this.areAdjacent(v, w)) {
                            nonAdj = true; 
                            break; 
                        }
                    } 
                    if (!nonAdj) {
                        this.clusters.get(j).add(v);
                        break;
                    }
                }
            }
        }
    }

    /**Prints the clusters.
     * @param out the PrintWriter
     */
    public void printClusters(PrintWriter out) {
        this.makeClusters(); 
        for (int i = 0; i < this.vertices; i++) {
            if (!this.clusters.get(i).isEmpty()) {
                out.print(i + 1 + ": ");
                for (int j = 0; j < this.clusters.get(i).size(); j++) {
                    out.print(this.clusters.get(i).get(j));
                    if (j < this.clusters.get(i).size() - 1) {
                        out.print(", ");
                    }
                }
                out.println();
            }
        }
    }

    /**Determine if two vertices are adjacent.
     * @param v1 the first vertex
     * @param v2 the second vertex
     * @return true if adjacent, else false
     */
    public boolean areAdjacent(Vertex v1, Vertex v2) {
        int search;
        if (this.adjList.get(v1.id()).size() < this.adjList.get(v2.id()).
                size()) {
            search = v1.id();
        } else {
            search = v2.id();
        }
        for (Edge e : this.adjList.get(search)) {
            if (e.getOther(v1) != null && e.getOther(v1).equals(v2)) {
                return true; 
            }
        }
        return false; 
    }
}
