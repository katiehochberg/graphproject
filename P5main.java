/*Kathryn Hochberg
 * khochbe1
 * 600.226
 * Project 5
 */

/*Efficiency Description
 * I decided to use an adjacency list implementation of the Graph class. I 
 * chose this because the other parts of the project I needed to know which 
 * other vertices each vertex was adjacent to, which made it easier to 
 * implement using an adjacency list. If I had used an adjacency matrix, the
 * extra space would have been wasted, and when I needed to find the vertices 
 * that were adjacent to each vertex I would have had to go through each index 
 * of the array to see if the array held an edge weight rather than 0. The 
 * first thing that I did was create the look-up table, which because it was 
 * just inserting vertices into an array, which is constant time for each 
 * insert, had a total time complexity of O(V) where V is the number of 
 * vertices. Then there was the creation and printing of the adjacency list, 
 * which had a time complexity of O(E) because each edge was inserted into the 
 * arrayList for both vertices that were on the edge, and then the list was 
 * printed which also ran in O(E). The next thing was the maximum spanning tree,
 * and in order to do this I used an arrayList of edges, which was sorted in 
 * O(ElogE) time, and then for each edge, two finds were called in the set 
 * class, and possibly a union. Therefore the total time complexity for the 
 * maximum spanning tree is O(ElogE). Since I used a list of edges that was 
 * already created as the input was passed into the graph class, there would 
 * have been no difference in the time complexity if I had used an adjacency 
 * matrix. To create the routing table I used Dijkstra's formula. For this I 
 * used an adaptable priority queue implemented by a binary heap. First each 
 * vertex is inserted into the priority queue, which for a binary heap runs in 
 * O(log V) time for each V. Then each vertex will need to be removed from the 
 * queue (as each one is visited) which will run in O(log V) time for each V. 
 * Then for each edge, you will potentially have to change the key (priority) 
 * for the other vertex, which for a binary heap runs in O(V) (I was unaware 
 * of the current priority and location of the node that needed a priority 
 * change). Therefore the time complexity of the creation of the routing table 
 * becomes O(V^3) since in the worst case, E = V^2. In order to create the 
 * clusters that contained non-adjacent edges, I used a priority queue, in order
 * to look at the vertices in descending degree. For each V that was removed
 * from the queue, I started in the first cluster, and if checked if each vertex
 * in that cluster was adjacent to the vertex removed from the queue. Since 
 * I used an adjacency list, the time complexity to find out if two vertices are
 * adjacent is O(degree(v)). Once there is a cluster in which it is not adjacent
 * then add it to that cluster. The worst case for degree(v) would be if it were
 * to equal V-1. If that were the case, then there would need to be separate 
 * clusters for each vertex, in which case the time complexity for making the 
 * clusters would be O(V^2).
 */

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;
import java.io.FileNotFoundException;

/**Driver program for Project 5 - Network Routing.*/
public final class P5main {

    /**Constructor for checkstyle.*/
    private P5main() {
    }
    
    /**Main for the driver program. 
     * @param args 
     * @throws FileNotFoundException 
     */
    public static void main(String[] args) throws FileNotFoundException {
        Graph graph = new Graph(); 
        String filename = "";
        Scanner kb = new Scanner(System.in); 
        System.out.print("Enter name of input file: ");
        filename = kb.nextLine();
        System.out.print("Enter name of output file: "); 
        String outputName = kb.nextLine(); 
        Scanner fromFile = new Scanner(new File(filename));
        String input = "";
        while (fromFile.hasNext()) {
            input = fromFile.nextLine();
            graph.newEntry(input);
        }
        PrintWriter out = new PrintWriter(outputName);
        out.println("Printing Look-Up Table...");
        graph.printLookUp(out);
        out.println("\nPrinting Adjacency Lists...");
        graph.printAdjList(out);
        out.println("\nFinding Max Bandwith Spanning Tree...");
        graph.maximumSpanningTree(out);
        out.println("\n\nPrinting Routing Table...");
        graph.routingTable(out);
        out.println("\nFinding clusters...");
        graph.printClusters(out);
        fromFile.close();
        kb.close();
        out.close();
    }
    
}
