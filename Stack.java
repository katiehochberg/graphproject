/*Kathryn Hochberg 
 * khochbe1
 * 600.226
 * Project 5
 */

//Taken from project 2.

/**Stack class. 
 * 
 * @author Kathryn Hochberg
 *
 * @param <T>
 */
public class Stack<T> {
    
    /*Implementing a single linked list - from SLList.java*/ 

    /** This class defines individual nodes which make up the linked list.
     *  This is a nested inner class, a member of SLList.
     */
    private final class Node {
        
        /** The data that goes inside a node. */
        T data;
        /** A reference to the next node in a list. */
        Node next;

        /** Create an empty node.
        */
        private Node() {
            this.data = null; 
            this.next = null; 
        }

        /** Create a node with data.
            @param val the value to put in the node
        */
        private Node(T val) {
            this.data = val;
        }

        /** Create a node with data and a node to follow.
            @param val the value to put in the node
            @param n the node to follow the new node
        */
        private Node(T val, Node n) {
            this.data = val;
            this.next = n;
        }
    }

    /** A reference to the first node in the list. */
    private Node head;
    
    /** Size of the stack. */
    private int size; 

    /** Create an empty list.
     */
    public Stack() {
        this.head = null; 
        this.size = 0;
    }
    
    /** Add an element to the top of the stack.
    @param data the element to add
     */
    public void push(T data) {
        /*Taken from SLList.java written in class.*/
        this.head = new Node(data, this.head);
        this.size++; 
    }

    /** Remove the top element from the stack.
    @throws StackEmptyException
    @return the element that was removed
     */
    public T pop() {
        /*Adapted from SLList.java written in class.*/
        if (this.isEmpty()) {
            throw new StackEmptyException(); 
        } 
        Node n = this.head; 
        this.head = this.head.next; 
        n.next = null; //cleanup 
        this.size--; 
        return n.data; 
    }

    /** Peek at the top element of the stack.
    @throws StackEmptyException
    @return the top
     */
    public T top() {
        if (this.isEmpty()) {
            throw new StackEmptyException(); 
        }
        return this.head.data; 
    }

    /** Find out how many elements are in the stack.
    @return the size
     */
    public int size() {
        return this.size; 
    }
    
    /**Get the head of the stack.
     * @return the head of the stack
     */
    public Node head() {
        return this.head; 
    }

    /** Find out if the stack is empty.
    @return true if empty, false otherwise
     */
    public boolean isEmpty() {
        return (this.size == 0); 
    }

    /** Create a description of the stack contents formatted in [ ], 
    starting with the bottom as the leftmost element, to the top.
    For example, a stack of integers might be [2, 3, 10, 5, 0] where
    2 is the bottom element and 0 is the top-most element.
    @return the description
     */
    public String toString() {
        String s = "[";
        Node curr = this.head;
        Node tail = null;
        while (curr != null) {
            tail = curr; 
            curr = curr.next; 
        }
        for (int i = 0; i < this.size; i++) {
            curr = this.head;
            s += tail.data; 
            if (curr != tail) {
                while (curr.next != tail) {
                    curr = curr.next; 
                }
                if (tail != this.head) {
                    s += ", ";
                }
                tail = curr; 
            }
        }
        s += "]";
        return s; 
    }
    
    /**Overrides the equals method. 
     * @param o the object that is being compared
     * @return true if equal, false if not
     */
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Stack<?>)) {
            return false; 
        }
        Stack<T> t = (Stack<T>) o;  
        if (t.size() != this.size) {
            return false;
        }
        Node curr1 = t.head();
        Node curr2 = this.head; 
        for (int i = 0; i < this.size; i++) {
            if (!((curr1.data).equals(curr2.data))) {
                return false; 
            }
            curr1 = curr1.next; 
            curr2 = curr2.next; 
        }
        return true; 
    }
    
    /**Overrides the hashCode method. 
     * @return the hashCode
     */
    @Override
    public int hashCode() {
        return this.toString().hashCode(); 
    }
}
