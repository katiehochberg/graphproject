/*Kathryn Hochberg
 * khochbe1
 * 600.226
 * Project 5
 */

/**Vertex class.*/
//Adapted from vertex class from class
public class Vertex {

    /**A counter used to give consecutive ID numbers 
     * to the vertices that are created.*/
    private static int nextID = 0;
    /**The ID number of the vertex.*/
    private int num; 
    /**The data associated with the vertex.*/
    private Object data;
    
    /**Vertex constructor.
     * @param d the object 
     */
    public Vertex(Object d) {
        this.data = d;
        this.num = nextID++;
    }

    /**Gets the id number of the vertex. 
     * @return the id number 
     */
    public int id() {
        return this.num;
    }

    /**Gets the data of the vertex.
     * @return the data
     */
    public Object data() {
        return this.data; 
    }
    
    /**Overrides the toString method. 
     * @return the string 
     */
    public String toString() {
        return this.num + "";
    }

    /**Overrides the equals method to see if 
     * two vertices are equal.
     * @param other the object being compared
     * @return true if equals, else false
     */
    public boolean equals(Object other) {
        if (other instanceof Vertex) {
            Vertex v = (Vertex) other;
            return this.num == v.num && this.data.equals(v.data);
        }
        return false;
    }
    
    /**Overrides the hashcode method.
     * @return the hashcode
     */
    public int hashCode() {
        return this.toString().hashCode(); 
    }
}
