/*Kathryn Hochberg
 * khochbe1
 * 600.226
 * Project 5
 */

import java.util.ArrayList;

/**Set implementation used for Graph Project.
 * @author Kathryn
 * @param <T>
 */
public class Set<T> {

    /**The parent of the set.*/
    Set<T> parent; 
    /**The data in the set.*/
    T data; 
    /**The size of the set.*/
    int size; 
    /**Array to hold the elements of the sets.*/
    Object[] sets; 
    
    /**Constructor for set. 
     * @param array the parents of the sets
     */
    public Set(ArrayList<T> array) {
        this.sets = new Object[array.size()];
        for (int i = 0; i < array.size(); i++) {
            if (array.get(i) != null) {
                this.sets[i] = new Set<T>(array.get(i));
            }
        }
    }

    /**Constructor for set. 
     * @param d the data
     */
    public Set(T d) {
        this.data = d; 
        this.size = 1; 
        this.parent = null; 
    }

    /**Find an item in the set, does path compression, 
     * and returns the parent. 
     * @param d the data
     * @return the parent
     */
    public Set<T> find(T d) {
        int i = 0; 
        while (i < this.sets.length && !(((Set<T>) this.sets[i]).data.equals(d)
                )) {
            i++;
        }
        Set<T> s = (Set<T>) this.sets[i]; 
        Stack<Set<T>> stack = new Stack<Set<T>>();
        while (s != null) {
            stack.push(s);
            s = s.parent; 
        }
        Set<T> p = stack.pop();
        while (!(stack.isEmpty())) {
            s = stack.pop(); 
            s.parent = p; 
        }
        return p; 
    }

    /**Union of two sets. 
     * @param d1 the data of the first set.
     * @param d2 the data of the second set. 
     * @return true if union successful, else false
     */
    public boolean union(T d1, T d2) {
        Set<T> p1 = this.find(d1);
        Set<T> p2 = this.find(d2);
        if (p1.data.equals(p2.data)) {
            return false;
        }
        if (p1.size <= p2.size) {
            p2.parent = p1; 
            p1.size += p2.size; 
            p2.size = -1; 
        } else {
            p1.parent = p2; 
            p2.size += p1.size; 
            p1.size = -1; 
        }
        return true; 
    }

}
