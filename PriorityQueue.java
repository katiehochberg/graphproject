/*Kathryn Hochberg
 * khochbe1
 * 600.226
 * Project 5
 */

/*Adapted from min priority queue written in class to an 
adaptable max priority queue.*/

import java.util.ArrayList;

/**Priority Queue class.
 * @author Kathryn
 * @param <T>
 */
public class PriorityQueue<T> {

    /**Inner node class of the priority queue.*/
    private final class Node implements Comparable<Node> {
        
        /**The first key of the node.*/
        private int key1; 
        /**The second key of the node.*/
        private int key2; 
         /**The first data member.*/
        private T data; 
        
        /**Node constructor.
         * @param k1 the first key
         * @param k2 the second key
         * @param v the data
         */
        public Node(int k1, int k2, T v) {
            this.key1 = k1;
            this.key2 = k2;
            this.data = v;
        }

        @Override
        public int compareTo(Node n) {
            if (this.key1 == n.key1) {
                return this.key2 - n.key2; 
            }
            return this.key1 - n.key1; 
        }
        
    }
    
    /**Array list used to hold the heap of the priority queue.*/
    private ArrayList<Node> priorityQueue;
    
    /**Constructor of the priority queue.*/
    public PriorityQueue() {
        this.priorityQueue = new ArrayList<Node>(); 
        this.priorityQueue.add(null);
    }
    
    /**Insert into the priority queue.
     * @param k1 the first key
     * @param k2 the second key
     * @param v the data
     */
    public void insert(int k1, int k2, T v) {
        Node n = new Node(k1, k2, v);
        this.priorityQueue.add(n);
        int curr = this.size(); 
        int rent = curr / 2; 
        Node temp; 
        while (rent > 0 && this.priorityQueue.get(rent).compareTo(
                this.priorityQueue.get(curr)) < 0) {
            temp = this.priorityQueue.get(rent);
            this.priorityQueue.set(rent, this.priorityQueue.get(curr));
            this.priorityQueue.set(curr, temp);
            curr = rent; 
            rent = rent / 2; 
        }
    }
    
    /**Checks to see if the priority queue is empty.
     * @return true if empty, else false
     */
    public boolean isEmpty() {
        if (this.size() == 0) {
            return true; 
        }
        return false; 
    }
       
    /**Get the size of the priority queue. 
     * @return the size 
     */
    public int size() {
        return this.priorityQueue.size() - 1; 
    }
    
    /**Change the key for an element.
     * @param d the data
     * @param k1 the first key
     * @param k2 the second key
     */
    public void changeKey(T d, int k1, int k2) {
        Node node = null; 
        int curr = 0; 
        for (Node n : this.priorityQueue) {
            if (n != null) {
                curr++; 
            }
            if (n != null && n.data.equals(d)) {
                node = n; 
                break; 
            }
        }
        if (node == null) {
            return; 
        }
        node.key1 = k1; 
        node.key2 = k2; 
        int rent = curr / 2; 
        if (rent == 0) {
            return; 
        }
        if (node.compareTo(this.priorityQueue.get(rent)) > 0) {
            this.bubbleUp(curr); 
        } else {
            this.bubbleDown(curr);
        }
    }
    
    /**Remove an element from the priority queue.
     * @return the array that holds the data and its previous data
     */
    public T remove() {
        if (this.isEmpty()) {
            return null; 
        }
        T t = this.priorityQueue.get(1).data; 
        Node last = this.priorityQueue.get(this.size());
        this.priorityQueue.remove(this.size());
        if (this.size() == 0) {
            return t; 
        }
        this.priorityQueue.set(1, last); 
        int curr = 1; 
        this.bubbleDown(curr);
        return t; 
    }
    
    /**Bubble up a element in the priority queue. 
     *@param curr the index of the current node
     */
    public void bubbleUp(int curr) {
        boolean swap = true; 
        int parent = curr / 2;  
        while (parent >= 1 && swap) {
            if (this.priorityQueue.get(curr).compareTo(this.priorityQueue.
                    get(parent)) > 0) {
                Node temp = this.priorityQueue.get(curr); 
                this.priorityQueue.set(curr, this.priorityQueue.get(parent));
                this.priorityQueue.set(parent, temp);
                curr = parent; 
                parent = curr / 2; 
            } else {
                swap = false; 
            }
        }
    }
    
    /**Bubble down an element in the priority queue.
     * @param curr the index of the current node
     */
    public void bubbleDown(int curr) {
        int left = curr * 2; 
        int right = curr * 2 + 1; 
        int max = 0; 
        boolean swap = true; 
        while (left < this.size() && swap) {
            max = this.findMaxChild(left, right);
            if (max != 0 && this.priorityQueue.get(curr).compareTo(this.
                    priorityQueue.get(max)) < 0) {
                Node temp = this.priorityQueue.get(curr);
                this.priorityQueue.set(curr, this.priorityQueue.get(max));
                this.priorityQueue.set(max, temp);
                curr = max; 
                left = curr * 2; 
                right = curr * 2 + 1;
            } else {
                swap = false; 
            }
        }
    }
    
    /**Used to find the max child. 
     * @param left the index of the left child
     * @param right the index of the right child
     * @return the index of the max child, 0 if no children 
     */
    public int findMaxChild(int left, int right) {
        int max = 0; 
        if (this.priorityQueue.get(left) != null && this.priorityQueue.get(
                right) != null) {
            if (this.priorityQueue.get(left).compareTo(this.priorityQueue.
                    get(right)) >= 0) {
                max = left; 
            } else {
                max = right; 
            }
        } else if (this.priorityQueue.get(left) != null) {
            max = left; 
        } else if (this.priorityQueue.get(right) != null) {
            max = right; 
        }
        return max; 
    }
    
    /**Prints the priority queue.
     */
    public void print() {
        System.out.println("Priority Queue:");
        for (Node n : this.priorityQueue) {
            if (n != null) {
                System.out.print(n.data + " ");
            }
        }
        System.out.println("");
    }
}
