/*Kathryn Hochberg
 * khochbe1
 * 600.226
 * Project 5
 */

import java.util.Comparator;

/**Edge class.*/
//Adapted from edge class from class. 

public class Edge implements Comparable<Edge>, Comparator<Edge> {

    /**The first vertex.*/
    private Vertex start;
    /**The second vertex.*/
    private Vertex end;
    /**The weight of the edge.*/
    private int weight; 

    /**Edge constructor. 
     * @param u the first vertex; 
     * @param v the second vertex; 
     * @param w the weight; 
     */
    public Edge(Vertex u, Vertex v, int w) {
        this.start = u;
        this.end = v;
        this.weight = w;
    }

    /**Checks to see if a vertex is part of the edge. 
     * @param v the vertex
     * @return true if it is in the edge, else false 
     */
    public boolean isIncident(Vertex v) {
        return this.start.equals(v) || this.end.equals(v);
    }

    /**Gets the starting vertex of the edge.
     * @return the starting vertex
     */
    public Vertex source() {
        return this.start;
    }

    /**Gets the ending vertex of the edge. 
     * @return the ending vertex
     */
    public Vertex end() {
        return this.end;
    }
    
    /**Gets the weight of the edge. 
     * @return the weight 
     */
    public int weight() {
        return this.weight; 
    }
    
    /**Gets the other vertex of an edge.
     * @param v the first vertex 
     * @return the other vertex, null if its not part of the edge
     */
    public Vertex getOther(Vertex v) {
        if (v.equals(this.start)) {
            return this.end; 
        } else if (v.equals(this.end)) {
            return this.start; 
        }
        return null; 
    }
    
    /**Overrides the toString method.
     * @return the string
     */
    public String toString() {
        return "(" + this.start + ", " + this.end + ", " + this.weight + ")";
    }

    @Override
    public int compare(Edge e0, Edge e1) {
        return -(e0.weight() - e1.weight());
    }

    @Override
    public int compareTo(Edge e0) {
        return e0.weight() - this.weight(); 
    }

}
