/*Kathryn Hochberg
 * khochbe1
 * 600.226
 * Project 5
 */

//Taken from project 2

/**StackEmptyException class to throw when trying to do an operation 
 * on an empty stack. 
 * @author Kathryn
 */
public class StackEmptyException extends RuntimeException {
   
    /**StackEmptyException constructor with no parameter.*/
    public StackEmptyException() {
        super("ERROR: stack is empty, invalid operation");
    }
    
    /**StackEmptyException constructor. 
     * @param err the error message
     */
    public StackEmptyException(String err) {
        super(err);
    }
}